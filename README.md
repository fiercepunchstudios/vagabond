# Vagabond #

v0.1.0

## What is this repository for? ##

* This repository tracks changes to the Vagrantfile used to produce https://atlas.hashicorp.com/fiercepunchstudios/boxes/vagabond
* That vagrant box is used for Fierce Punch Studios dev work with Ruby on Rails.
* It has rbenv, ruby, rails, bundler, postgresql and sqlite installed, as well as bash and vim customizations.

## How do I get set up? ##

### Summary of set up ###
This guide assumes a Windows host; when working on Linux or OSX, you may already have these components installed. If so, skip the applicable steps.

### Pre-requisites ###

1. Install [Git for Windows](https://msysgit.github.io/).
2. Install [ConEmu](http://conemu.github.io/), and configure it to use the Git Bash shell that came with Git for Windows.
3. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads).
4. Install [Vagrant](http://www.vagrantup.com/downloads).
5. Install the [Atom Editor](https://atom.io/).

## How do I start a project?

### When working on a project that exists in a remote repository ###

If that project was set up with Vagabond, it will have its own Vagrantfile in the repository. You just need to pull the project and get started as normal.

1. Start ConEmu, navigate to where you want the project to live (I like C:\Users\<me>\Projects), and `git pull <project-url>`
2. Run `vagrant up`. This will take longer the first time, because it has to create the VM and run all the first-time setup provisioning.
3. Run `vagrant ssh`
4. Open Atom
5. Press Ctrl+Shift+O to open the Open Folder dialog.
6. Navigate to the folder that contains your project and click the `Select Folder` button.

Now you should have one window with ConEmu running your VM and another with your whole project open in Atom.

### When working on a new project ###
1. Using ConEmu as your console emulator, navigate to the directory of your project and enter `vagrant init fiercepunchstudios/vagabond`. This creates a Vagrantfile at the root of your project.
2. Enter `vagrant up` to create the virtual machine and start it running.
3. Enter `vagrant ssh` to start a shell session inside your virtual machine.
4. In the `~/.gitconfig` file, change the `name = YOUR NAME` and `email = YOUR@EMAIL.com` to reflect your credentials.
5. Change the `database.yml` file to `username: <%= ENV[DB_USER_DEV] %>` and `password: <%= ENV[DB_PASS_DEV] %>`. This will allow your application to use the generic development postgres user that vagabond created for you.
6. Back in Windows, use Atom to make changes to your project files. This will automatically be reflected in the `/vagrant` directory of your virtual machine.
7. When you're ready to start a web server for testing, navigate to the root of your project in ConEmu and type `bers`. It's short for `bundle execute rails server -b 0.0.0.0`. You can now visit your rails project in your Windows browser of choice at `localhost:3000`.

### Optional Configuration ###
* Copy your SSH credentials into the VM.
* Add `config.vm.hostname = your_host_name` to the Vagrantfile.
* Specify CPU cores and RAM with `config.vm.provider "virtualbox" do |v|
  v.memory = 1024
  v.cpus = 2
end`

## Who do I talk to? ##

* Repo admin: justin@fiercepunchstudios.com

## Admin Notes ##

### Packaging Box ###

1. `vagrant package --output vagabond_x.y.z.box --vagrantfile vagabond_runtime.rb`