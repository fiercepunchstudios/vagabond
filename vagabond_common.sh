#!/bin/bash

echo $'\nrunning vagabond_common.sh'
####################################
# set locale
sudo /usr/sbin/update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8

####################################
# apt-get
sudo apt-get -qq update 1> /dev/null
sudo apt-get -qq install git build-essential 1> /dev/null

####################################
# Docker
# config.vm.provision :shell, inline: <<-DOCKER
#   wget -qO- https://get.docker.com/ | sh
#   sudo usermod -aG docker vagrant
#   # Add exceptions to UFW, if using it
# DOCKER

####################################
# Bash configuration
export VAGABOND_HEADER="# vagrant fiercepunchstudios/vagabond config below:"

####################################
# git
if [ -e ~/.gitconfig ]; then
  echo '~/.gitconfig exists; skipping.'
else
  # TODO: import ~/.gitconfig from host
  # TODO: import ~/.gitignore_global from host

  echo 'Configuring git.'

  git config --global color.ui true
  git config --global user.name "YOUR NAME"
  git config --global user.email "YOUR@EMAIL.com"
  git config --global core.excludesfile ~/.gitignore_global

  git config --global alias.st 'status -s'
  git config --global alias.ci "commit"
  git config --global alias.br "branch"
  git config --global alias.co "checkout"
  git config --global alias.df "diff"
  git config --global alias.lg "log -p"
  git config --global alias.hist "log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short"
  git config --global alias.type "cat-file -t"
  git config --global alias.dump "cat-file -p"
  git config --global alias.unstage "reset HEAD --"
  git config --global alias.last "log -l HEAD"
fi

if [ -e ~/.gitignore_global ]; then
  echo '~/.gitignore_global exists; skipping.'
else
  wget -q https://raw.githubusercontent.com/github/gitignore/master/Rails.gitignore -O ~/.gitignore_global
  echo "$VAGABOND_HEADER" >> ~/.gitignore_global
  echo '.idea' >> ~/.gitignore_global
  echo '.vagrant' >> ~/.gitignore_global
fi

####################################
# SSH
# TODO: import ~/.ssh from host

####################################
# Bash interactive shell; ~/.bashrc
if (grep -qF "$VAGABOND_HEADER" ~/.bashrc); then
  echo '~/.bashrc already configured; skipping.'
else
  echo 'Configuring .bashrc.'

  echo $'\n'
  echo "$VAGABOND_HEADER" >> .bashrc
  echo "bind 'set completion-ignore-case on'" >> .bashrc

  # git PS1 prompt
  echo "# http://blog.backslasher.net/git-prompt-variables.html" >> .bashrc
  echo "GIT_PS1_SHOWDIRTYSTATE=1 # displays * or +" >> .bashrc
  echo "GIT_PS1_SHOWSTASHSTATE=1 # displays $" >> .bashrc
  echo "GIT_PS1_SHOWUNTRACKEDFILES=1 # displays %" >> .bashrc
  echo "GIT_PS1_SHOWUPSTREAM='auto' # displays <, >, <> or =" >> .bashrc
  echo "GIT_PS1_SHOWCOLORHINTS=1" >> .bashrc
  echo "GIT_PS1_DESCRIBE_STYLE='contains'" >> .bashrc

  echo 'RESET="\[\033[0m\]"' >> .bashrc
  echo 'GRAY="\[\033[1;30m\]"' >> .bashrc
  echo 'PURPLE="\[\033[0;35m\]"' >> .bashrc
  echo 'BEFORE_GIT_PS1="$GRAY\t \u@\h:$PURPLE\w$RESET"' >> .bashrc
  echo 'AFTER_GIT_PS1="$GRAY\n\! \$ $RESET"' >> .bashrc
  echo 'GIT_PS1_CMD='"'"'__git_ps1 "$BEFORE_GIT_PS1" "$AFTER_GIT_PS1"'"'" >> .bashrc
  echo 'PROMPT_COMMAND="$GIT_PS1_CMD"' >> .bashrc

  echo 'cd /vagrant' >> .bashrc

  source ~/.bashrc
fi

####################################
# vim
if [ -e ~/.vimrc ]; then
  echo '~/.vimrc already exists; skipping.'
else
  #TODO: import ~/.vimrc
  echo 'Configuring vim.'

  echo 'set ignorecase' >> ~/.vimrc
  echo 'set number' >> ~/.vimrc
  echo 'set shiftwidth=4' >> ~/.vimrc
  echo 'set softtabstop=4' >> ~/.vimrc
  echo 'set incsearch' >> ~/.vimrc
  echo 'set title' >> ~/.vimrc
fi
