#!/bin/bash

echo $'\nrunning vagabond_mean.sh'

####################################
# nvm
if [ -e ~/.nvm ]; then
  echo '~/.nvm exists; skipping.'
else
  curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash > /dev/null
  echo '[ -s "~/.nvm/nvm.sh" ] && . "~/.nvm/nvm.sh"' >> ~/.profile
  source ~/.nvm/nvm.sh
  nvm install 5.0 --quiet --silent &> /dev/null
  nvm use 5.0
  nvm alias default 5.0


####################################
# bower & grunt
# npm install -g bower grunt-cli > /dev/null

####################################
# Mongo

####################################
# Express

####################################
# Angular

fi

# digital ocean recommends meanjs over mean.io
# https://www.digitalocean.com/community/tutorials/how-to-install-a-mean-js-stack-on-an-ubuntu-14-04-server
