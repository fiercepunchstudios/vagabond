#!/bin/bash

echo $'\nrunning vagabond_rails.sh'

# heroku prerequisites from: https://toolbelt.heroku.com/install-ubuntu.sh
# if [ ! -e /etc/apt/sources.list.d/heroku.list ]
# echo "deb http://toolbelt.heroku.com/ubuntu ./" | sudo tee --append /etc/apt/sources.list.d/heroku.list
# wget -q -O- https://toolbelt.heroku.com/apt/release.key | sudo apt-key add -
# fi
if (command -v heroku >/dev/null); then
  echo 'heroku.list already exists; skipping.'
else
  echo 'Installing heroku.'
  wget -O- --quiet https://toolbelt.heroku.com/install-ubuntu.sh | sh &> /dev/null
fi

#!/bin/bash
sudo apt-get -qq update > /dev/null
sudo apt-get -qq install libssl-dev libyaml-dev sqlite3 libsqlite3-dev postgresql postgresql-common libpq-dev libffi-dev libreadline-dev > /dev/null

# Bash non-interactive; ~/.profile
# for rails psql
export VAGABOND_HEADER="# vagrant fiercepunchstudios/vagabond config below:"
if (grep -qF "$VAGABOND_HEADER" ~/.profile); then
  echo '~/.profile already modified by Vagabond; skipping.'
else
  echo 'Configuring .profile.'

  echo "$VAGABOND_HEADER" >> .profile
  echo "alias be='bundle exec'" >> .profile
  echo "alias bers='be rails server -b 0.0.0.0'" >> .profile
  echo 'export DB_USER_DEV=db_user_dev' >> .profile
  echo 'export DB_PASS_DEV=unique_hashed_gibberish' >> .profile
  source ~/.profile
fi

####################################
# postgres
echo 'Configuring Postgres'
sudo -u postgres psql -c "create role $DB_USER_DEV with createdb login password '$DB_PASS_DEV';"

####################################
# rbenv
if [ -e ~/.rbenv ]; then
  echo '~/.rbenv exists; skipping.'
else
  echo 'Installing rbenv, ruby, rails and bundler.'

  git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.profile
  echo 'eval "$(rbenv init -)"' >> ~/.profile
  echo '# rbenv config in .profile instead of .bashrc.' >> ~/.bashrc
  source .profile

  git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
  rbenv install 2.2.0
  rbenv global 2.2.0

  echo 'Installing gems.'
  echo "gem: --no-ri --no-rdoc" > $HOME/.gemrc
  gem install rails --version 4.2.1 > /dev/null
  gem install bundler --version 1.10.3 > /dev/null
  rbenv rehash
fi
