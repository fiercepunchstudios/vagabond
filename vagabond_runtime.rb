Vagrant.configure(2) do |config|
  config.vm.network "forwarded_port", guest: 3000, host: 3000

  config.vm.provision "file", source: '~/.ssh', destination: '~/.ssh'
end
